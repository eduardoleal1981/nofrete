### Execução local do projeto, usando docker
- **script startDocker**
    - `docker build -t tserverimage:latest .`
    - `docker run --name tServer -p 8080:8080 tserverimage:latest`
  
### Clean das imagens e containers docker
- **script cleanDocker**
    - `docker stop tServer`
    - `docker rm tServer`
    - `docker rmi tserverimage`
    - `docker system prune --force`